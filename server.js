// =================================================================
// get the packages we need ========================================
// =================================================================
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const configDB = require('./config/database'); // get our config file
const config = require('./config/auth'); // get our config file
// =================================================================
// configuration ===================================================
// =================================================================
mongoose.Promise = require('bluebird');
mongoose.connect(configDB.url, {
    useMongoClient: true,
    /* other options */
}); // connect to database
app.set('superSecret', config.secretOrPublicKey); // secret variable
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
//passport
const passport = require('passport');
require('./config/passport')(passport);
app.use(passport.initialize());
// use morgan to log requests to the console
app.use(morgan('dev'));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, access_token');
    next();
})
// allow to get files from directory
app.use("/public", express.static(__dirname + '/public'));
// middleware to set user
const userController = require('./app/controllers/user');
app.use((req, res, next) => {
    if (req.method === 'OPTIONS') {
        res.status(200).send({
            status: true,
            raport: 'CORS allowed'
        });
    } else {
        let token = req.body.access_token || req.query.access_token || req.headers.access_token;
        //let token = req.headers.access_token;
        console.log('---===<<< 1 auth middleware >>>===---');
        if (token) {
            userController.decodeToken(token).then((res) => {
                if (res.status) {
                    req.user = {
                        access_token: res.data
                    }
                    req.decoded = res;
                }
                next();
            }).catch((err) => {
                userController.handleError(err);
                next();
            });
        } else {
            next();
        }
    }
});

//routing
app.use('/', express.Router()).get('', function (req, res) {
    console.log('display API DOC')
    res.sendFile(__dirname + '/public/index2.html');
})

app.use('/staticApiDoc', (req, res) => {
    console.log('display API DOC')
    res.sendFile(__dirname + '/public/index.html');
})

app.use('/genApiDoc', (req, res) => {
    const Docs = require('express-api-doc');
    const appCpy = Object.assign(app); // your app.js
    const docs = new Docs(appCpy);
    docs.generate({
        path: './public/index2.html',
    });
})

const project = express.Router();
require('./app/routes/project')(project, passport);
app.use('/project', project);

const file = express.Router();
require('./app/routes/file')(file, passport);
app.use('/file', file);

const user = express.Router();
require('./app/routes/user')(user, passport);
app.use('/user', user);
// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
const runMsg = 'Magic happens at';
console.log(runMsg + ' http://localhost:' + port);
// =================================================================
// start the ws server =============================================
// =================================================================
const webSocketServer = require('./serverWS')
webSocketServer(5000, runMsg);