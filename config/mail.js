var nodemailer = require('nodemailer');

module.exports = {
    /**
     * Send mail based on preconfigured transport object
     * 
     * @param  mailOptions - Object that contain data to send mail
     * @param mailOptions.to - Reciver or receivers
     * @param mailOptions.subject - Subject of msg
     * @param mailOptions.html - Data in html fortmat that will be send to reciver
     */
    sendMailMsg: (mailOptions) => {
        // config 4 gmail
        // var transporter = nodemailer.createTransport({
        //     service: 'Gmail',
        //     auth: {
        //         user: 'example@gmail.com', // Your email id
        //         pass: 'password' // Your password
        //     }
        // });
        const transporter = nodemailer.createTransport({
            host: 'mail_host_addr',
            port: 587,
            secure: false, // use SSL
            tls: {
                rejectUnauthorized: false
            },
            auth: {
                user: 'email',
                pass: 'pass'
            }
        });

        // set mail addres that msg will be send
        mailOptions.from = transporter.auth.user;
        mailOptions.html = mailOptions.html + '<br><br> Full content email disclaimer example ;)';

        return new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    reject(error)
                } else {
                    resolve(info);
                };
            });
        })
    },
};