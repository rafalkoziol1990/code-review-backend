var JwtBearerStrategy = require('passport-http-jwt-bearer');
var secretOrPublicKey = require('./auth').secretOrPublicKey;

var User = require('../app/models/user').User;

module.exports = function (passport) {

    passport.use(new JwtBearerStrategy(secretOrPublicKey,
        (token, done) => {
            User.findById(token.data._id, (err, user) => {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                //additionary condition is tokens matches
                if (token = user.token) {
                    return done(null, user, token);
                } else {
                    return done(null, false, {
                        message: 'Tokens do not match.'
                    });
                }
            });
        }
    ));
};