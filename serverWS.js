/*WEBSOCKET */
// const express = require('express');
const http = require('http');
const url = require('url');
const WebSocket = require('ws');
const app = require('express');
const server = http.createServer(app);
const wss = new WebSocket.Server({
    server
});
const userController = require('./app/controllers/user');
const projectController = require('./app/controllers/project');

module.exports = (portNumber, runMsg) => {
    wss.on('connection', connection = (ws, req) => {

        ws.on('message', function incoming(message) {
            const msgData = JSON.parse(message);
            userController.decodeToken(msgData.jwt)
                .then((res) => {
                    if (res.status) {
                        /* AUTHENTICATED */
                        console.log('received: %s', message);
                        projectController.pushMsgToDB(JSON.parse(message))
                            .then((res) => {
                                wss.clients.forEach((client, i) => {
                                    client.send(message);
                                });
                            })
                            .catch((err) => {
                                projectController.handleError(err);
                            })
                        /* AUTHENTICATED */
                    }
                }).catch((err) => {
                    userController.handleError(err);
                })
        });

        ws.on('close', (client) => {
            console.log('Client lost', client)
        });
    });

    server.listen(portNumber, listening = () => {
        console.log(runMsg + ' ws://localhost:' + server.address().port);
    });
}