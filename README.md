# auth-api-server
This repo uses JWT.
Authentication by verifying a token using Express route middleware.

## Requirements
- node and npm

## Usage
1. Clone the repo: `git clone <repo path>`
2. Install dependencies: `npm install`
3. Change SECRET in `config folder`
4. Start the server: `nodemon server.js`
5. Display Api doc by visiting: `http://localhost:8080`

## Usefull
1. Refresh express-api-doc by going to `http://localhost:8080/genApiDoc`
2. Static in at `http://localhost:8080/staticApiDoc`


