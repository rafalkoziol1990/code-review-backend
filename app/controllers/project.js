const Model = require('./../models/project').Model;

const projectController = {
    /**
     * Handle error from project routes
     * @param {Object} err - Object that contain error 
     */
    handleError: (err) => {
        console.log(err);
    },

    /**
     * Handle error from project routes
     * @param {Object} msg - Object that contain chat msg, unique user login and id
     */
    pushMsgToDB: (msg) => {
        return new Promise((resolve, reject) => {
            Model.findById(msg.project, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    const msgTmp = {
                        data: msg.data,
                        user: {
                            login: msg.user.login,
                            id: msg.user.id
                        },
                        added: new Date()
                    };
                    if (data.messages) {
                        data.messages.push(msgTmp)
                    } else {
                        data.messages = [msgTmp];
                    }
                    data.save(function (err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data);
                        }
                    });
                }
            });
        })
    }
}

module.exports = projectController;