const jwt = require('jsonwebtoken');
const secretOrPublicKey = require('../../config/auth').secretOrPublicKey;
const UserModel = require('./../models/user').User;

const userController = {
    /**
     * Return promise with object taht contains data about auth state
     * @param {string} token - JWT token
     */
    decodeToken: (token) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, secretOrPublicKey, (err, user) => {
                if (!err) {
                    resolve({
                        status: true,
                        data: user
                    });
                } else {
                    reject({
                        status: false,
                        data: err
                    });
                }
            })
        })
    },

    /**
     * Handle error from user routes and methods that contains user auth policy
     * @param {Object} err - Object that contain error 
     */
    handleError: (err) => {
        console.log(err);
    },

    /**
     * Auth every route under this def
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} next - Express middleware callback
     * @param {Object} next - Express middleware callback
     * @param {Object} passport - Passport object
     */
    isAuth: (req, res, next, passport) => {
        console.log('auth middleware');
        const url = req.originalUrl;
        let token = req.body.access_token || req.query.access_token || req.headers.access_token;
        if (token && passport.authenticate('jwt-bearer', {
                session: false
            })) {
            jwt.verify(token, secretOrPublicKey.secretOrPublicKey, function (err, decoded) {
                if (err) {
                    res.status(403).send({
                        err: err
                    })
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(401).send({
                err: 'No token provided'
            });
        }
    },

    isTokenActual: ((token, userId) => {
        return new Promise((resolve, reject) => {
            UserModel.findById(userId, (err, user) => {
                if (err || !user) {
                    if (!user) {
                        err = 'No such user in db!'
                    }
                    reject({
                        data: err,
                        status: false
                    })
                } else {
                    if (user.token === token) {
                        resolve({
                            data: user.token,
                            status: true
                        })
                    } else {
                        reject({
                            data: 'Token is not actual',
                            status: false
                        })
                    }
                }
            });
        })
    })

}

module.exports = userController;