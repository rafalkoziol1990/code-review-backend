//4 sharp we need to install python 2.7 or greater and v c++ 2005
// and enable .NET
// a) Press “Windows Logo” + “R” keys on the keyboard.
// b) Type “appwiz.cpl” in the “Run” command box and press “ENTER”.
// c) In the “Programs and Features” window, click on the link “Turn Windows features on or off”.
// d) Check if the “.NET Framework 3.5 (includes .NET 2.0 and 3.0)” option is available in it.
// e) If yes, then enable it and then click on “OK”.
// f) Follow the on-screen instructions to complete the installation and restart the computer, if prompted.
// or
// update npm and npm install --global --production windows-build-tools
// https://github.com/mpayetta/node-es6-rest-api/blob/master/server/controllers/tasks.js

// fluent ffmpeg need some extra installation like 
// https://ffmpeg.org/download.html
// https://www.npmjs.com/package/fluent-ffmpeg
// http://www.wikihow.com/Install-FFmpeg-on-Windows

const shelljs = require('shelljs');
const fs = require('fs');
const sizeOf = require('image-size');
const sharp = require('sharp');
const ffmpeg = require('fluent-ffmpeg');

const fileController = {
    sizes: [
        { prefix: 's', maxWidth: 320 },
        { prefix: 'm', maxWidth: 480 },
        { prefix: 'l', maxWidth: 720 },
    ],

    makePathIfDoesNotExist: (path) => {
        if (!fs.exists(path)) {
            shelljs.mkdir('-p', path);
        }
    },

    sharpIt: (file) => {
        return new Promise((resolve, reject) => {
            let loopCounter = 0;
            let pathToImage = file.destination + '/' + file.filename;
            if (file.mimetype === 'video/mp4' || file.mimetype === 'video/avi') {
                pathToImage = file.destination + '/' + file.tmp + '.png';
            }
            fileController.sizes.forEach((v, i) => {
                const maxWidth = v.maxWidth;
                const prefix = v.prefix;
                //create path if does not exist
                const path = file.destination + '/' + prefix
                fileController.makePathIfDoesNotExist(path);
                // set source element
                sharp(pathToImage)
                    .resize(maxWidth)
                    .jpeg({quality:73})
                    .max()
                    .toFile(path + '/' + file.filename, (err, info) => {
                        if (err) {
                            reject(err);
                        }
                    });
                //callback
                loopCounter++;
                if (loopCounter === fileController.sizes.length) {
                    resolve(sizeOf(pathToImage));
                }
            })
        })
    },

    thumbnailFromMovie: (file) => {
        return new Promise((resolve, reject) => {
            new ffmpeg(file.destination + '/' + file.filename)
                .screenshots({
                    count: 1, // if U set 4 will take screens at 20%, 40%, 60% and 80% of the video 
                    timemarks: ['0'], // number of seconds
                    filename: file.tmp
                }, file.destination).on('error', function(err) {
                    reject(err);
                }).on('end', function() {
                    fileController.sharpIt(file).then((sharpItResolve) => {
                        resolve(sharpItResolve);
                    }).catch((err) => {
                        fileController.handleError(err);
                    });
                });
        })
    },

    /**
     * Handle error from files routes
     * @param {Object} err - Object that contain error 
     */
    handleError: (err) => {
        console.log(err);
    }
}

module.exports = fileController;