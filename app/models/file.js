const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fileSchema = mongoose.Schema({
    originalname: { type: String, required: true },
    filename: { type: String, required: true },
    tmp: { type: String },
    mimetype: { type: String },
    size: { type: String },
    keyword: { type: String },
    parentid: { type: String },
    destination: { type: String },
    info: {
        width: { type: String },
        height: { type: String },
        type: { type: String }
    },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    deleted: { type: Boolean, default: false },
}, { timestamps: true });

const File = mongoose.model('File', fileSchema);
const Models = { File: File };
module.exports = Models;