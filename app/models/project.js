const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const modelSchema = mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    finish: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    tickets: [{}],
    contractors: [{}],
    messages: [{}]
}, {
    timestamps: true
});


const Model = mongoose.model('Project', modelSchema);
const Models = {
    Model: Model
};
module.exports = Models;