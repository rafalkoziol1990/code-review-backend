const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

// set up a mongoose model
const userSchema = mongoose.Schema({
    admin: Boolean,
    avatar: String,
    login: {
        type: String,
        required: false,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    token: String,
    address: {
        street: String,
        post: String,
        city: String
    },
    phone: {
        type: String 
    },
    firstname: String,
    friends: [],
    lastname: String,
}, {
    timestamps: true
});

userSchema.methods.generateJwtToken = (userData, secretOrPublicKey, expire) => {
    //set up  jwt token
    return jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (expire), //24 hours expire time
        data: userData
    }, secretOrPublicKey);
};

userSchema.methods.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};

userSchema.methods.validPassword = (password, passwordFromDB) => {
    return bcrypt.compareSync(password, passwordFromDB);
};
 
const User = mongoose.model('User', userSchema);
const Models = {
    User: User 
};

module.exports = Models;