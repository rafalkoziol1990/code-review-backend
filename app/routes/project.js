const Model = require('../models/project').Model;
const projectController = require('../controllers/project');
const userController = require('./../controllers/user');

module.exports = function (router, passport) {
    /**
     * Auth every route under this def
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} next - Express middleware callback
     */
    router.use((req, res, next) => {
        if (req.hasOwnProperty('decoded') && req.decoded.status) {
            let token = req.body.access_token || req.query.access_token || req.headers.access_token;
            userController.isTokenActual(token, req.decoded.data.data._id).then((res) => {
                next()
            }).catch((err) => {
                res.status(401).send({
                    err: err.data
                })
            })
        } else {
            res.status(401).send({
                err: 'No token or token incorect'
            })
        }
    });

    /**
     * Create empty document and return it
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     */
    router.post('/add', (req, res) => {
        const model = new Model({
            user: req.user.access_token.data._id
        });
        model.save(function (err, data) {
            if (err) {
                projectController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                res.status(200).send(data);
            }
        });
    });

    /**
     * Update object with passed data
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} req.body - Object where keys are the same as those in mongoose schema
     */
    router.post('/update/:id', (req, res) => {
        Model.findOne({
            _id: req.params.id
        }, (err, data) => {
            if (err) {
                projectController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                const model = data;
                Object.keys(req.body).forEach((v, i) => {
                    data[v] = req.body[v];
                })
                model.save((err, data) => {
                    if (err) {
                        projectController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        res.status(200).send(data);
                    }
                });
            }
        });
    });

    /**
     * Fetch data based on passed pagination and query
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} req.body.query - Object that contains query part (custom created on in fx body)
     * @param {Object} req.body.pagination - Object that contains pagination part (page, limit, total)
     */
    router.post('/fetchAll', (req, res) => {
        const logedUser = req.user.access_token.data;
        const q = req.body;
        const query = {
            active: q.query.active,
            deleted: q.query.deleted
        };
        if (q.query.title) {
            query.title = new RegExp(q.query.title, 'gi')
        }

        const userQuery = {
            $or: [{
                'user': logedUser._id
            }, {
                'contractors.user': logedUser.login
            }]
        };

        const totalQuery = {
            $and: [query, userQuery]
        };

        console.log(JSON.stringify(totalQuery, null, 4));

        Model.find(totalQuery, (err, data) => {
                if (err) {
                    projectController.handleError(err);
                    res.status(404).send({
                        raport: err,
                        status: false
                    });
                } else {
                    Model.count(totalQuery, function (err, count) {
                        if (err) {
                            projectController.handleError(err);
                        } else {
                            q.pagination.total = count;
                            res.status(200).send({
                                data: data,
                                query: q
                            });
                        }
                    });

                }
            })
            .populate({
                path: 'user',
                model: 'User',
                select: 'login avatar'
            })
            .skip(q.pagination.page * q.pagination.limit)
            .limit(q.pagination.limit)
            .select(q.select)
            .sort(q.sort);
    });

    /**
     * Fetch single document from database
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} req.body.query - If object is specified then returned object contain fields defined in req.body.query.select
     */
    router.post('/fetchRow/:id', (req, res) => {
        Model.findById(req.params.id, (err, data) => {
            if (err) {
                projectController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                res.status(200).send(data);
            }
        }).select(req.body.query ? req.body.query.select : 'title description user finish tickets contractors messages')
    });

    router.delete('/delete/:id', function (req, res) {
        Model.findOne({
            _id: req.params.id
        }, (err, data) => {
            if (err) {
                projectController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                data.deleted = true;
                data.save((err, data) => {
                    if (err) {
                        fileController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        res.status(200).send(data);
                    }
                });
            }
        });
    });
};