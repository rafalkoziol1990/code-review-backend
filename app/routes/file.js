const File = require('../models/file').File;
const multer = require('multer'); /*4 upload*/
const fs = require('fs');
const fileController = require('../controllers/file');
const userController = require('./../controllers/user');

module.exports = (router) => {
    /**
     * Auth every route under this def
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} next - Express middleware callback
     */
    router.use((req, res, next) => {
        if (req.hasOwnProperty('decoded') && req.decoded.status) {
            let token = req.body.access_token || req.query.access_token || req.headers.access_token;
            userController.isTokenActual(token, req.decoded.data.data._id).then((res) => {
                next()
            }).catch((err) => {
                res.status(401).send({
                    err: err.data
                })
            })
        } else {
            res.status(401).send({
                err: 'No token or token incorect'
            })
        }
    });

    const storage = multer.diskStorage({
        destination: (req, file, callback) => {
            const userid = req.user.access_token.data._id;
            const keyword = req.query.keyword;
            const parentid = req.query.parentid;
            const path = 'public/uploads/' + userid + '/' + keyword + '/' + parentid;
            fileController.makePathIfDoesNotExist(path);
            callback(null, path);
        },
        filename: (req, file, callback) => {
            callback(null, Date.now() + '-' + file.originalname.toLowerCase());
        }
    });

    const upload = multer({
        storage: storage
    }).array('file');

    /**
     * Fetch single document from database
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {array} req.files - Array that contains object with file data
     * @param {string} req.query.parentid - String passed in url
     * @param {string} req.query.keyword - String passed in url
     */
    router.post('/upload', (req, res) => {
        upload(req, res, (err) => {
            const parentid = req.query.parentid;
            const keyword = req.query.keyword;

            if (err) {
                fileController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            }
            // request.files is an object where fieldname is the key and value is the array of files 
            req.files.forEach((v, i) => {
                let file = new File({
                    originalname: req.files[i].originalname,
                    filename: req.files[i].filename,
                    mimetype: req.files[i].mimetype,
                    tmp: req.files[i].filename.split('.')[0],
                    size: req.files[i].size,
                    keyword: keyword,
                    parentid: parentid,
                    destination: req.files[i].destination,
                    user: req.user.access_token.data._id
                });
                new Promise((resolve) => {
                    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
                        fileController.sharpIt(file).then((resolveSharpIt) => {
                            resolve(resolveSharpIt);
                        }).catch((err) => {
                            fileController.handleError(err);
                        });
                    } else if (file.mimetype === 'video/mp4' || file.mimetype === 'video/avi') {
                        fileController.thumbnailFromMovie(file).then((resolveThumbnailFromMovie) => {
                            resolve(resolveThumbnailFromMovie);
                        }).catch((err) => {
                            fileController.handleError(err);
                        });
                    } else {
                        resolve({});
                    }
                }).then((resolve) => {
                    file.info = resolve;
                    file.save((err, data) => {
                        if (err) {
                            fileController.handleError(err);
                            res.status(404).send({
                                raport: err,
                                status: false
                            });
                        } else {
                            res.status(200).send(data);
                        }
                    });
                }).catch((err) => {
                    fileController.handleError(err);
                });
            })

        });
    });

    /**
     * Fetch single document from database
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} req.body.query - Object that contains query part (custom created on in fx body)
     * @param {Object} req.body.pagination - Object that contains pagination part (page, limit, total)
     */
    router.post('/fetchAll', (req, res) => {
        const q = req.body;
        File.find({
                $and: [q.query, {
                    'deleted': false
                }]
            }, (err, data) => {
                if (err) {
                    fileController.handleError(err);
                    res.status(404).send({
                        raport: err,
                        status: false
                    });
                } else {
                    res.status(200).send({
                        data: data,
                        query: q
                    });
                }
            })
            .skip(q.pagination.page * q.pagination.limit)
            .limit(q.pagination.limit)
            .select(q.select)
            .sort(q.sort);
    });

    /**
     * Fetch single document from database
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} req.body.query - If object is specified then returned object contain fields defined in req.body.query.select
     */
    router.post('/fetchRow/:id', (req, res) => {
        File.findOne({
            _id: req.params.id
        }, (err, data) => {
            if (err) {
                fileController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                res.status(200).send(data);
            }
        })
    });


    router.delete('/delete/:id', (req, res) => {
        File.findOne({
            _id: req.params.id
        }, (err, file) => {
            if (err) {
                fileController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                file.deleted = true;
                file.save((err, data) => {
                    if (err) {
                        fileController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        res.status(200).send(data);
                    }
                });
                // fs.exists(data.destination + '/' + data.filename, (exists) => {
                //     File.remove({
                //         _id: data._id
                //     }, (err) => {
                //         if (err) {
                //             fileController.handleError(err);
                //             res.status(404).send({
                //                 raport: err,
                //                 status: false
                //             });
                //         } else {
                //             if (exists) {
                //                 fs.unlinkSync(data.destination + '/' + data.filename);
                //             } else {
                //                 fileController.handleError('File not found, so not deleting.');
                //             }
                //             res.status(200).send({
                //                 status: true
                //             });
                //         }
                //     });
                // });
            }
        });
    });

};