const User = require('../models/user').User; // get our mongoose model
const jwt = require('jsonwebtoken');
const secretOrPublicKey = require('../../config/auth');
const mail = require('../../config/mail');
const userController = require('./../controllers/user');

module.exports = (router, passport) => {
    /**
     * Auth every route under this def
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {Object} next - Express middleware callback
     */
    router.use((req, res, next) => {
        console.log('---===<<< 2 auth middleware >>>===---');
        const url = req.originalUrl;
        if (url === '/user/login' || url === '/user/registry' || url === '/user/restorePassword' || url === '/user/isAuthenticate') {
            next();
        } else if (req.hasOwnProperty('decoded') && req.decoded.status) {
            let token = req.body.access_token || req.query.access_token || req.headers.access_token;
            userController.isTokenActual(token, req.decoded.data.data._id).then((res) => {
                next()
            }).catch((err) => {
                res.status(401).send({
                    err: err.data
                })
            })
        } else {
            res.status(401).send({
                err: 'No token or token incorect'
            })
        }
    })

    /**
     * Put new user in db if it pass
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.login - Client email
     * @param {string} req.body.password - Client password
     */
    router.post('/registry', (req, res) => {
        User.findOne({
            login: req.body.login
        }, (err, data) => {
            if (err) {
                userController.handleError(err);
                res.status(500).send({
                    raport: err,
                    status: false
                });
            } else {
                if (data) {
                    res.status(200).send({
                        raport: 'User already exist in DB',
                        status: false
                    });
                } else {
                    const user = new User();
                    user.login = req.body.login;
                    user.password = user.generateHash(req.body.password);
                    user.admin = false;
                    user.save((err, data) => {
                        if (err) {
                            userController.handleError(err);
                            res.status(500).send({
                                raport: err,
                                status: false
                            });
                        } else {
                            res.status(200).send({
                                raport: data,
                                status: true
                            });
                        }
                    });
                }
            }
        })
    });

    /**
     * Search for mail in db and send mail with new password if user exist
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.login - Client email
     */
    router.post('/restorePassword', (req, res) => {
        User.findOne({
            login: req.body.login
        }, (err, data) => {
            if (err) {
                userController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                const restoredPasswordPrep = new Date().getTime() + data._id;
                const restoredPassword = restoredPasswordPrep.substring(0, 32);
                data.password = data.generateHash(restoredPassword);
                //saveing user new token
                data.save((err, savedData) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        mail.sendMailMsg({
                            to: data.login, // list of receivers
                            subject: 'Restore password 4 ' + data.login, // Subject line
                            html: '<span>Yours new password is: <b>' + restoredPassword + '</b></span>' // You can choose to send an HTML body instead
                        }).then((res) => {
                            res.status(200).send({
                                mailSend: res,
                                status: true
                            });
                        }).catch((err) => {
                            res.status(200).send({
                                mailSend: err,
                                status: false
                            });
                        });
                    }
                });
            }
        });
    });

    /**
     * Search for mail in db and send mail with new password if user exist
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.login - Client email
     * @param {string} req.body.password - Client password
     */
    router.post('/login', (req, res) => {
        User.findOne({
            login: req.body.login
        }, (err, data) => {
            if (err || !data) {
                if (!data) {
                    err = 'No such user in db! Create new then sign in.'
                }
                userController.handleError(err);
                res.status(200).send({
                    raport: err,
                    status: false
                });
            } else {
                if (data.validPassword(req.body.password, data.password)) {
                    data.token = data.generateJwtToken({
                        _id: data._id,
                        login: data.login,
                        created: new Date().getTime()
                    }, secretOrPublicKey.secretOrPublicKey, 24 * 60 * 60);
                    //saveing user new token
                    data.save((err, savedData) => {
                        if (err) {
                            userController.handleError(err);
                            res.status(200).send({
                                raport: err,
                                status: false
                            });
                        } else {
                            res.status(200).send({
                                access_token: savedData.token,
                                status: true
                            });
                        }
                    });
                } else {
                    res.status(200).send({ //401
                        raport: '401 Unauthorized ',
                        status: false
                    });
                }
            }
        });
    });

    /**
     * logout=gen new token with 0s to expire
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.login - Client email
     * @param {string} req.body.password - Client password
     */
    router.get('/logout', (req, res) => {
        User.findById(req.decoded.data._id, (err, data) => {
            if (err) {
                userController.handleError(err);
                res.status(404).send({
                    raport: err,
                    status: false
                });
            } else {
                data.token = data.generateJwtToken(req.decoded.data, secretOrPublicKey.secretOrPublicKey, 0);
                //saveing user new token wits 0s to expire
                data.save((err, savedData) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        res.status(200).send({
                            access_token: savedData.token,
                            status: true
                        });
                    }
                });
            }
        });
    });

    /**
     * Update all passed data. Iterate on object keys and asign new values
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.userData - It is json that contain user data.
     */
    router.post('/changeUserData', (req, res) => {
        jwt.verify(req.headers.access_token, secretOrPublicKey.secretOrPublicKey, (err, decoded) => {
            if (err) {
                res.status(200).send({
                    data: 'Unauthenticated',
                    status: false
                });
            } else {
                User.findById(decoded.data._id, (err, data) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(200).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        const userData = JSON.parse(req.body.userData);
                        Object.keys(userData).forEach((v, i) => {
                            data[v] = userData[v];
                        })
                        data.save((err, savedData) => {
                            if (err) {
                                userController.handleError(err);
                                res.status(200).send({
                                    raport: err,
                                    status: false
                                });
                            } else {
                                res.status(200).send({
                                    data: savedData,
                                    status: true
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    /**
     * Push new elemnt into friends array
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.friend - friend's email address
     */
    router.post('/addNewFriend', (req, res) => {
        jwt.verify(req.headers.access_token, secretOrPublicKey.secretOrPublicKey, (err, decoded) => {
            if (err) {
                res.status(200).send({
                    data: 'Unauthenticated',
                    status: false
                });
            } else {
                User.findById(decoded.data._id, (err, data) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        const friend = req.body.friend;
                        if (data.friends) {
                            if (data.friends.indexOf(friend) === -1) {
                                data.friends.push(friend);
                            }
                        } else {
                            data.friends = [friend];
                        }
                        data.save((err, savedData) => {
                            if (err) {
                                userController.handleError(err);
                                res.status(404).send({
                                    raport: err,
                                    status: false
                                });
                            } else {
                                res.status(200).send({
                                    data: savedData,
                                    status: true
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    /**
     * Replace old client password by hashed value of new one
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     * @param {string} req.body.password - actual client password   
     * @param {string} req.body.newPassword - password that client want to set
     */
    router.post('/changePassword', (req, res) => {
        jwt.verify(req.headers.access_token, secretOrPublicKey.secretOrPublicKey, (err, decoded) => {
            if (err) {
                res.status(200).send({
                    data: 'Unauthenticated',
                    status: false
                });
            } else {
                User.findById(decoded.data._id, (err, data) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        if (data.validPassword(req.body.password)) {
                            data.password = data.generateHash(req.body.newPassword);
                            //saveing user data
                            data.save((err, savedData) => {
                                if (err) {
                                    userController.handleError(err);
                                    res.status(404).send({
                                        raport: err,
                                        status: false
                                    });
                                } else {
                                    res.status(200).send({
                                        access_token: savedData.token,
                                        status: true
                                    });
                                }
                            });
                        } else {
                            res.status(200).send({
                                raport: '401 Unauthorized',
                                status: false
                            });
                        }
                    }
                });
            }
        });
    });

    /**
     * Return user data and status with positive value
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     */
    router.get('/isAuthenticate', (req, res) => {
        jwt.verify(req.headers.access_token, secretOrPublicKey.secretOrPublicKey, (err, decoded) => {
            if (err) {
                userController.handleError(err);
                res.status(200).send({
                    data: 'Unauthenticated - token corupted',
                    status: false
                });
            } else {
                User.findById(decoded.data._id, (dbErr, user) => {
                    if (dbErr || !user) {
                        res.status(200).send({
                            data: 'No user found',
                            status: false
                        });
                    } else {
                        if (user.token === req.headers.access_token) {
                            res.status(200).send({
                                data: decoded,
                                status: true
                            });
                        } else {
                            res.status(200).send({
                                data: 'It is not actual token',
                                status: false
                            });
                        }
                    }
                })
            }
        });
    });

    /**
     * Return user data 
     * @param {Object} req - Express request object
     * @param {Object} res - Express response object
     */
    router.get('/getUserData', (req, res) => {
        jwt.verify(req.headers.access_token, secretOrPublicKey.secretOrPublicKey, (err, decoded) => {
            if (err) {
                res.status(200).send({
                    data: 'Unauthenticated',
                    status: false
                });
            } else {
                User.findById(decoded.data._id, (err, data) => {
                    if (err) {
                        userController.handleError(err);
                        res.status(404).send({
                            raport: err,
                            status: false
                        });
                    } else {
                        delete data.password;
                        res.status(200).send(data);
                    }
                });
            }
        })
    });
};